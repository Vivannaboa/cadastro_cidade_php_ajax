	$(document).ready(function() {

	    //Aqui a ativa a imagem de load
	    function loading_show() {
	        $('#loading').html("<img src='img/loading.gif'/>").fadeIn('fast');
	    }

	    //Aqui desativa a imagem de loading
	    function loading_hide() {
	        $('#loading').fadeOut('fast');
	    }


	    // aqui a fun��o ajax que busca os dados em outra pagina do tipo html, n�o � json
	    function load_dados(valores, page, div) {
	        $.ajax({
	            type: 'POST',
	            dataType: 'json',
	            url: page,
	            beforeSend: function() { //Chama o loading antes do carregamento
	                loading_show();
	            },
	            data: valores,
	            success: function(dados) {
	                loading_hide();
	                $(div).empty(); //limpa a tela
	                for (var i = 0; dados.length > i; i++) {
	                    //Adicionando registros retornados na tabela
	                    $(div).append(
	                        '<tr><td>' + dados[i].CODIGO +
	                        '</td><td>' + dados[i].DESCRICAO +
	                        '</td><td>' + dados[i].UF +
	                        '<td data-id="' + dados[i].CODIGO + '">' +
	                        '<button data-toggle="modal" data-target="#edit-item" class="btn btn-primary edit-item">Editar</button>' +
	                        '<button class="btn btn-danger remove-item">Excluir</button>' +
	                        '</td></tr>'
	                    );
	                }
	            },
	            error: function() {
	                loading_hide();
	                $(div).empty();
	                $(div).append("Nenhum registro encontrado!")
	            }

	        });
	    }



	    //Aqui eu chamo o metodo de load pela primeira vez sem parametros para pode exibir todos
	    load_dados(null, 'php/pesquisa.php', '#tabela');


	    //Aqui uso o evento key up para começar a pesquisar, se valor for maior q 0 ele faz a pesquisa
	    $('#pesquisa').keyup(function() {

	        var valores = $('#form_pesquisa').serialize() //o serialize retorna uma string pronta para ser enviada

	        //pegando o valor do campo #pesquisa
	        var $parametro = $(this).val();

	        if ($parametro.length >= 1) {
	            load_dados(valores, 'php/pesquisa.php', '#tabela');
	        } else {
	            load_dados(null, 'php/pesquisa.php', '#tabela');
	        }
	    });

	    /* Create new Item */
	    $(".crud-submit").click(function(e) {
	        e.preventDefault();
	        var form_action = $("#create-item").find("form").attr("action");
	        var codigo = $("#create-item").find("input[name='codigo']").val();
	        var descricao = $("#create-item").find("input[name='descricao']").val();
	        var uf = $("#create-item").find("select[name='uf']").val();
	        var valores = $('#form_pesquisa').serialize();

	        if (codigo != '' && descricao != '' && uf != '') {
	            $.ajax({
	                dataType: 'json',
	                type: 'POST',
	                url: form_action,
	                data: { codigo: codigo, descricao: descricao, uf: uf }
	            }).done(function(data) {
	                $("#create-item").find("input[name='codigo']").val('');
	                $("#create-item").find("input[name='descricao']").val('');
	                $(".modal").modal('hide');
	                toastr.success('Cidade cadastrada com sucesso.', 'Sucesso', { timeOut: 5000 });

	                load_dados(valores, 'php/pesquisa.php', '#tabela');

	            });
	        } else {
	            alert('Você esqueceu o código, descrição ou UF.')
	        }


	    });

	    /* Remove Item */
	    $("body").on("click", ".remove-item", function() {
	        var id = $(this).parent("td").data('id');
	        var c_obj = $(this).parents("tr");

	        $.ajax({
	            dataType: 'json',
	            type: 'POST',
	            url: 'php/excluir.php',
	            data: { id: id }
	        }).done(function(data) {
	            c_obj.remove();
	            toastr.success('Item Removido com sucesso.', 'Sucesso', { timeOut: 5000 });
	        });

	    });


	    /* Edit Item */
	    $("body").on("click", ".edit-item", function() {

	        var id = $(this).parent("td").data('id');
	        var codigo = $(this).parent("td").prev("td").prev("td").prev("td").text();
	        var descricao = $(this).parent("td").prev("td").prev("td").text();
	        var uf = $(this).parent("td").prev("td").text();

	        $("#edit-item").find("input[name='codigo']").val(codigo);
	        $("#edit-item").find("input[name='descricao']").val(descricao);
	        $("#edit-item").find("select[name='uf']").val(uf);
	        $("#edit-item").find(".edit-id").val(id);

	    });


	    /* Updated new Item */
	    $(".crud-submit-edit").click(function(e) {

	        e.preventDefault();
	        var form_action = $("#edit-item").find("form").attr("action");
	        var codigo = $("#edit-item").find("input[name='codigo']").val();
	        var descricao = $("#edit-item").find("input[name='descricao']").val();
	        var uf = $("#edit-item").find("select[name='uf']").val();
	        var id = $("#edit-item").find(".edit-id").val();
	        var valores = $('#form_pesquisa').serialize();

	        if (codigo != '' && descricao != '' && uf != '') {
	            $.ajax({
	                dataType: 'json',
	                type: 'POST',
	                url: form_action,
	                data: { codigo: codigo, descricao: descricao, uf: uf, id: id }
	            }).done(function(data) {
	                $(".modal").modal('hide');
	                toastr.success('A cidade foi atualizad com sucesso.', 'Sucesso', { timeOut: 5000 });
	                load_dados(valores, 'php/pesquisa.php', '#tabela');
	            });
	        } else {
	            alert('Você esqueceu o código, descrição ou UF.')
	        }
	    });
	});