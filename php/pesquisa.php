<?php
//recebemos nosso parametro vindo do form
$parametro = isset($_POST['pesquisa']) ? $_POST['pesquisa'] : null;

//pega a classe de conexão
require_once('../class/Conexao.class.php');

try {
	
	$pdo = new Conexao();
	
	$resultado = $pdo->select("SELECT * FROM municipio WHERE descricao LIKE '$parametro%' ORDER BY descricao ASC");
	
	$pdo->desconectar();
	
}
catch (PDOException $e){
	
	echo $e->getMessage();
	
}

//resgata os dados na tabela
if(count($resultado)){
	
	foreach ($resultado as $res) {
		
		$vetor[] = array_map('utf8_encode', $res);
		
	}
	
}

echo json_encode($vetor);

?>